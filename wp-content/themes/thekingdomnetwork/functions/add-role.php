<?php

function add_roles_on_axstudio()
{
    add_role('client', 'Client', array('read' => true, 'edit_posts' => false));
}

register_activation_hook(__FILE__, 'add_roles_on_axstudio');