<?php
/*
Template Name: Front Page
*/

get_header();
?>
    <main class="frontpage page" id="main">

        <section class="section">
            <div class="frontpage--main">
                <div class="top">
                    <p class="small">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                </div>
                <div class="frontpage--main_container">
                    <h1 class="title subtitle">Learn, connect, find a mentor or service.</h1>
                    <!--                   <h1 class="mainintro">The Kingdom<br/>Network</h1>-->
                    <div class="frontpage__title">
                        <svg class="herotitle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 525.9 190.2">
                            <defs>
                                <style>.cls-1 {
                                        font-size: 97.14px;
                                        fill: #1d1d1b;
                                        font-family: Casta-Thin, Casta;
                                        font-weight: 200;
                                        letter-spacing: 0.01em;
                                    }

                                    .cls-2 {
                                        letter-spacing: 0.03em;
                                    }

                                    .cls-3 {
                                        letter-spacing: 0.01em;
                                    }

                                    .cls-4 {
                                        letter-spacing: 0.03em;
                                    }

                                    .cls-5 {
                                        letter-spacing: 0.01em;
                                    }

                                    .cls-6 {
                                        letter-spacing: 0.01em;
                                    }

                                    .cls-7 {
                                        letter-spacing: 0.02em;
                                    }

                                    .cls-8 {
                                        letter-spacing: 0.01em;
                                    }</style>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                                <g id="Layer_1-2" data-name="Layer 1">
                                    <text class="cls-1" transform="translate(0 82.57)">T
                                        <tspan class="cls-2" x="42.16" y="0">he</tspan>
                                        <tspan class="cls-3" x="170.77" y="0">K</tspan>
                                        <tspan class="cls-4" x="224.39" y="0">in</tspan>
                                        <tspan class="cls-5" x="296.17" y="0">g</tspan>
                                        <tspan class="cls-6" x="349.31" y="0">d</tspan>
                                        <tspan class="cls-2" x="397.49" y="0">om</tspan>
                                        <tspan x="0" y="85">N</tspan>
                                        <tspan class="cls-4" x="53.43" y="85">e</tspan>
                                        <tspan class="cls-7" x="105.1" y="85">t</tspan>
                                        <tspan class="cls-8" x="134.93" y="85">w</tspan>
                                        <tspan class="cls-2" x="201.56" y="85">o</tspan>
                                        <tspan class="cls-7" x="253.92" y="85">r</tspan>
                                        <tspan class="cls-2" x="288.31" y="85">k</tspan>
                                    </text>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <div class="row">
                        <div class="col-md-12 frontpage__search">
                            <h1 class="title-xs">I am looking for </h1>
                            <form class="frontpage__search_form" action="/" method="get">
                                <input type="text" name="s" id="search" value="<?php the_search_query(); ?>"
                                       placeholder=""/>
                                <input type="image" alt="Search"
                                       src="http://thekingdom.development/wp-content/uploads/2020/08/iconfinder_search_172546.svg"/>
                            </form>
                        </div>
                </div>
                    <!--                       <div class="col-md-3 intro">-->
                    <!--                           <p class="sub">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor-->
                    <!--                               incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>-->
                    <!--                       </div>-->
                </div>

            </div>
        </section>


        <section class="frontpage-about">
            <div class="frontpage-about__image">
                <div class="bg"
                     style="background-image: url('http://thekingdom.development/wp-content/uploads/2020/08/andrew-neel-QLqNalPe0RA-unsplash-scaled.jpg'); background-size: cover;">
                </div>
            </div>
            <div class="frontpage-about__text">
                <h2 class="text-align-center">Helping you collaborate anytime, whenever, wherever.</h2>
            </div>


        </section>


        <section class="frontpage-services">
                <div class="frontpage-services__banner">
                    <div class="frontpage-services__banner_text">
                        <h2>Start connecting</h2>
                    </div>
                </div>
                <div class="frontpage-services__wrapper">
                    <div class="frontpage-services__wrapper__service">
                        <div class="title">
                            <h2>Business</h2>
                        </div>
                        <div class="image">
                            <div class="bg"
                                 style="background-image: url('https://dev.thekingdom.network/wp-content/uploads/2020/08/annie-spratt-MChSQHxGZrQ-unsplash-scaled.jpg'); background-size: cover;">
                            </div>
                        </div>

                    </div>
                    <div class="frontpage-services__wrapper__service">
                        <div class="title">
                            <h2>Mentor</h2>
                        </div>
                        <div class="image">
                            <div class="bg"
                                 style="background-image: url('https://dev.thekingdom.network/wp-content/uploads/2020/08/charles-deluvio-rRWiVQzLm7k-unsplash-scaled.jpg'); background-size: cover;">
                            </div>
                        </div>

                    </div>
                    <div class="frontpage-services__wrapper__service">
                        <div class="title">
                            <h2>Learning</h2>
                        </div>
                        <div class="image">
                            <div class="bg"
                                 style="background-image: url('https://dev.thekingdom.network/wp-content/uploads/2020/08/christina-wocintechchat-com-fw1uaNHTVcE-unsplash-scaled.jpg'); background-size: cover;">
                            </div>
                        </div>

                    </div>

                </div>
        </section>


    </main>


<?php
get_footer();
