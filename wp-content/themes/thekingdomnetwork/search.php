<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package AX_studio
 */

get_header();
?>


    <main class="page pageresult" id="main">

        <section class="page-services">
            <div class="page-services__banner">
                <div class="page-services__banner_text">
                    <h2><?php echo get_search_query(); ?></h2>
                </div>
            </div>
            <div class="page-services__wrapper">

                <div class="page-services__wrapper__service">
                    <div class="title">
                        <h3>AX Studio</h3>
                        <p>Southsea Portsmouth, UK</p>
                    </div>
                    <div class="image">
                        <div class="bg"
                             style="background-image: url('https://dev.thekingdom.network/wp-content/uploads/2020/08/andrew-neel-QLqNalPe0RA-unsplash-scaled.jpg'); background-size: cover;">
                        </div>
                    </div>

                </div>

                <div class="page-services__wrapper__service">
                    <div class="title">
                        <h3>Open Cloud Media</h3>
                        <p>Southsea Portsmouth, UK</p>
                    </div>
                    <div class="image">
                        <div class="bg"
                             style="background-image: url('https://dev.thekingdom.network/wp-content/uploads/2020/08/charles-deluvio-rRWiVQzLm7k-unsplash-scaled.jpg'); background-size: cover;">
                        </div>
                    </div>

                </div>
                <div class="page-services__wrapper__service">
                    <div class="title">
                        <h3>Creative Little Clouds</h3>
                        <p>Southsea Portsmouth, UK</p>
                    </div>
                    <div class="image">
                        <div class="bg"
                             style="background-image: url('https://dev.thekingdom.network/wp-content/uploads/2020/08/christina-wocintechchat-com-fw1uaNHTVcE-unsplash-scaled.jpg'); background-size: cover;">
                        </div>
                    </div>

                </div>
                <div class="page-services__wrapper__service">
                    <div class="title">
                        <h3>Wild Pursuit</h3>
                        <p>Southsea Portsmouth, UK</p>
                    </div>
                    <div class="image">
                        <div class="bg"
                             style="background-image: url('https://dev.thekingdom.network/wp-content/uploads/2020/08/annie-spratt-MChSQHxGZrQ-unsplash-scaled.jpg'); background-size: cover;">
                        </div>
                    </div>

                </div>

                <div class="page-services__wrapper__service">
                    <div class="title">
                        <h3>AX Studio</h3>
                        <p>Southsea Portsmouth, UK</p>
                    </div>
                    <div class="image">
                        <div class="bg"
                             style="background-image: url('https://dev.thekingdom.network/wp-content/uploads/2020/08/andrew-neel-QLqNalPe0RA-unsplash-scaled.jpg'); background-size: cover;">
                        </div>
                    </div>

                </div>

                <div class="page-services__wrapper__service">
                    <div class="title">
                        <h3>Open Cloud Media</h3>
                        <p>Southsea Portsmouth, UK</p>
                    </div>
                    <div class="image">
                        <div class="bg"
                             style="background-image: url('https://dev.thekingdom.network/wp-content/uploads/2020/08/charles-deluvio-rRWiVQzLm7k-unsplash-scaled.jpg'); background-size: cover;">
                        </div>
                    </div>

                </div>
                <div class="page-services__wrapper__service">
                    <div class="title">
                        <h3>Creative Little Clouds</h3>
                        <p>Southsea Portsmouth, UK</p>
                    </div>
                    <div class="image">
                        <div class="bg"
                             style="background-image: url('https://dev.thekingdom.network/wp-content/uploads/2020/08/christina-wocintechchat-com-fw1uaNHTVcE-unsplash-scaled.jpg'); background-size: cover;">
                        </div>
                    </div>

                </div>
                <div class="page-services__wrapper__service">
                    <div class="title">
                        <h3>Wild Pursuit</h3>
                        <p>Southsea Portsmouth, UK</p>
                    </div>
                    <div class="image">
                        <div class="bg"
                             style="background-image: url('https://dev.thekingdom.network/wp-content/uploads/2020/08/annie-spratt-MChSQHxGZrQ-unsplash-scaled.jpg'); background-size: cover;">
                        </div>
                    </div>

                </div>


            </div>
        </section>


    </main>


<?php
get_footer();
