<?php
/*
Template Name: Result Page
*/

get_header();
?>
    <main class="page pageresult" id="main">

        <section class="page-services">
                <div class="page-services__banner">
                    <div class="page-services__banner_text">
                        <h2>Business</h2>
                    </div>
                </div>
                <div class="page-services__wrapper">

                    <div class="page-services__wrapper__service">
                        <div class="title">
                            <h3>AX Studio</h3>
                            <p>Southsea Portsmouth, UK</p>
                        </div>
                        <div class="image">
                            <div class="bg"
                                 style="background-image: url('http://thekingdom.development/wp-content/uploads/2020/08/annie-spratt-MChSQHxGZrQ-unsplash-scaled.jpg'); background-size: cover;">
                            </div>
                        </div>

                    </div>

                    <div class="page-services__wrapper__service">
                        <div class="title">
                            <h3>Open Cloud Media</h3>
                            <p>Southsea Portsmouth, UK</p>
                        </div>
                        <div class="image">
                            <div class="bg"
                                 style="background-image: url('http://thekingdom.development/wp-content/uploads/2020/08/annie-spratt-MChSQHxGZrQ-unsplash-scaled.jpg'); background-size: cover;">
                            </div>
                        </div>

                    </div>
                    <div class="page-services__wrapper__service">
                        <div class="title">
                            <h3>Creative Little Clouds</h3>
                            <p>Southsea Portsmouth, UK</p>
                        </div>
                        <div class="image">
                            <div class="bg"
                                 style="background-image: url('http://thekingdom.development/wp-content/uploads/2020/08/annie-spratt-MChSQHxGZrQ-unsplash-scaled.jpg'); background-size: cover;">
                            </div>
                        </div>

                    </div>
                    <div class="page-services__wrapper__service">
                        <div class="title">
                            <h3>Wild Pursuit</h3>
                            <p>Southsea Portsmouth, UK</p>
                        </div>
                        <div class="image">
                            <div class="bg"
                                 style="background-image: url('http://thekingdom.development/wp-content/uploads/2020/08/annie-spratt-MChSQHxGZrQ-unsplash-scaled.jpg'); background-size: cover;">
                            </div>
                        </div>

                    </div>


                </div>
        </section>


    </main>


<?php
get_footer();
