gsap.registerPlugin(ScrollTrigger);


let container = document.getElementById("main");

gsap.to(container, {
    x: () => -(container.scrollWidth - document.documentElement.clientWidth) + "px",
    ease: "none",
    scrollTrigger: {
        trigger: container,
        invalidateOnRefresh: true,
        pin: true,
        scrub: 1,
        end: () => "+=" + container.offsetWidth
    }
})


// gsap.to(".frontpage-about__text", {
//     scrollTrigger: {
//         trigger : ".frontpage-about",
//         toggleActions: "restart reverse restart reverse",
//         start : "100px top",
//     },
//     duration: 3,
//     opacity : 1,
//     x: 0
// });



// var splitText = splitText('.herotitle');
var frontpageMain = gsap.timeline();

frontpageMain.from(".herotitle", {
    duration:2,
    opacity: 0
});


frontpageMain.from(".subtitle", {
    duration: 1,
    y: -200,
    ease: "back"
});

// frontpageMain.to(".header", {
//     duration: 0.5,
//     left: 0
// });

