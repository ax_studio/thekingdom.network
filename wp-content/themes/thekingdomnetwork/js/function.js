function textareaAutoGrow() {


    $('textarea').attr('rows', '1');

    $('textarea').each(function () {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
}

function onScroll() {
    // $(window).scroll(function () {
    //
    //     if ($(this).scrollTop() > 10) {
    //         $('.header').addClass('change');
    //     } else {
    //         $('.header').removeClass('change')
    //     }
    // });
}


function scrollToTop() {
    $('.header-title__text').on('click', () => {
        $('html,body').animate({
            scrollTop: 0
        },500);
    });
}

function menuToggle() {
    $('.menu-wrapper').click(function() {
        $(this).toggleClass('active');
    });

    $('.plus').click(function() {
        var icon = $(this);
        if(icon.hasClass('toCross')) {
            icon.removeClass('toCross');
            icon.addClass('toPlus');
        } else {
            icon.removeClass('toPlus');
            icon.addClass('toCross');
        }
    });

}